package com.example.lovemeter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button b_compare;
    ImageView iv_needle;
    EditText et_yourname, et_othername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_compare = (Button) findViewById(R.id.b_compare);
        iv_needle = (ImageView) findViewById(R.id.iv_needle);
        et_yourname = (EditText) findViewById(R.id.et_yourname);
        et_othername = (EditText) findViewById(R.id.et_othername);

        b_compare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String yourName = et_yourname.getText().toString();
                String otherName = et_othername.getText().toString();

                int totalLetters = yourName.length() + otherName.length();
                int totalMatches = 0;

                for(int i = 0; i < yourName.length(); i++) {
                    for(int j = 0; j < otherName.length(); j++) {
                        if (yourName.charAt(i) == otherName.charAt(j)) {
                            totalMatches++;
                        }
                    }
                }

                for(int i = 0; i < otherName.length(); i++) {
                    for(int j = 0; j < yourName.length(); j++) {
                        if (otherName.charAt(i) == yourName.charAt(j)) {
                            totalMatches++;
                        }
                    }
                }

                float compareScore = (float) totalMatches / totalLetters;
                int loveScore = ((int) compareScore * 100);
                RotateAnimation ra = new RotateAnimation(-25, loveScore * (float) 2.5,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                ra.setFillAfter(true);
                ra.setDuration(2000);
                ra.setInterpolator(new AccelerateDecelerateInterpolator());
                iv_needle.startAnimation(ra);
                Toast.makeText(MainActivity.this, "Love Score: " + loveScore, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
